import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


public class Problem3UnitTest {
    public static final String SYMMETRIC = "symmetric";
    public static final String NOT_SYMMETRIC = "not symmetric";
    public static final String NOT_POSSIBLE = "not possible";

    private static ProblemSolution ps;
    @BeforeClass
    public static void setup(){
        ps=new ProblemSolution();
    }

    @Test
    public void testSymmetry1(){
        assertEquals(NOT_POSSIBLE, ps.isSymmetric(new String[]{"1","1","1","1","<>","0","1","0","1","<>","1","1","0","1"}));
    }

    @Test
    public void testSymmetry2(){
        assertEquals(SYMMETRIC, ps.isSymmetric(new String[]{"1","0","1","<>","0","1","0","<>","1","0","1"}));
    }

    @Test
    public void testSymmetry3(){
        assertEquals(NOT_SYMMETRIC, ps.isSymmetric(new String[]{"1","0","1","<>","0","1","0","<>","1","14","1"}));
    }

    @Test
    public void testSymmetry4(){
        assertEquals(NOT_POSSIBLE, ps.isSymmetric(new String[]{"1","0","1","0","1","0","<>","1","14","1"}));
    }

    @Test
    public void testSymmetry5(){
        assertEquals(NOT_SYMMETRIC, ps.isSymmetric(new String[]{"1","141","1","<>","0","1","0","<>","1","14","1"}));
    }
}
