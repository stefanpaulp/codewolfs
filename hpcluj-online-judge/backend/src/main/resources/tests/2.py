from UUTModule import *
import unittest
class StringEncodeTests(unittest.TestCase):
    def testEncode1(self):
        self.assertEquals("1234123@#$%@", encode("abcd123@#$%@"))


    def testEncode2(self):

        self.assertEquals("117918106   9151723106  119911811", encode("aqirjf   ioqwjf  asik811"))


    def testEncode3(self):

        self.assertEquals("!@#$%^&*()@#@$@!@  %$&*", encode("!@#$%^&*()@#@$@!@  %$&*"))


    def testEncode4(self):

        self.assertNotEquals("abcdadasda", encode("abcdadasda"))


    def testEncode5(self):

        self.assertEquals("1234567891011121314151617181920212223242526", encode("abcdefghijklmnopqrstuvwxyz"))



def main():
    unittest.main()

if __name__ == '__main__':
    main()