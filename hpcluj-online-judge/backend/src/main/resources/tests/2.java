import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


public class Problem2UnitTest {

    private static ProblemSolution ps;
    @BeforeClass
    public static void setup(){
        ps=new ProblemSolution();
    }

    @Test
    public void testEncode1(){
        assertEquals("1234123@#$%@", ps.encode("abcd123@#$%@"));
    }

    @Test
    public void testEncode2(){
        assertEquals("117918106   9151723106  119911811", ps.encode("aqirjf   ioqwjf  asik811"));
    }

    @Test
    public void testEncode3(){
        assertEquals("!@#$%^&*()@#@$@!@  %$&*", ps.encode("!@#$%^&*()@#@$@!@  %$&*"));
    }

    @Test
    public void testEncode4(){
        assertNotEquals("abcdadasda", ps.encode("abcdadasda"));
    }

    @Test
    public void testEncode5(){
        assertEquals("1234567891011121314151617181920212223242526", ps.encode("abcdefghijklmnopqrstuvwxyz"));
    }
}
