package com.hp.tools.judge.utils;

import com.hp.tools.judge.services.AgentService;
import com.hp.tools.judge.services.JudgeService;

import java.util.concurrent.Executors;

/**
 * @author Octavian
 * @since 06.03.2015
 */
public class Constants {
    public static final int MAX_THREADS = 10;
    public static final JudgeService JUDGE_SERVICE = new JudgeService();
    public static final AgentService AGENT_SERVICE = new AgentService(new ConnectionDetails("http", "127.0.0.1", 8081, "agent/test", 5000, 120000), Executors.newFixedThreadPool(MAX_THREADS));
    public static final String PROBLEMS_FOLDER = "problems/";
    public static final String TESTS_FOLDER = "tests/";
}
