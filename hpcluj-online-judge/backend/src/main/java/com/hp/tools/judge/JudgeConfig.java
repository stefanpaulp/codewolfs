package com.hp.tools.judge;

import com.hp.tools.judge.utils.CORSResponseFilter;
import com.wordnik.swagger.jaxrs.config.BeanConfig;
import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;

import javax.ws.rs.ApplicationPath;

/**
 * @author Octavian
 * @since 02.02.2015
 */
@ApplicationPath("/judge/api")
public class JudgeConfig extends ResourceConfig {

    public JudgeConfig() {
        // integration with Swagger
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setBasePath("/judge/api");
        beanConfig.setResourcePackage("com.hp.tools.judge.web");

        beanConfig.setTitle("Online Judge API");
        beanConfig.setDescription("Use the Online Judge API for creating users, reading problems and evaluating solutions");
        beanConfig.setVersion("1.0");

        beanConfig.setLicense("Apache License, Version 2.0");
        beanConfig.setLicenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html");
        beanConfig.setScan(true);

        packages("com.wordnik.swagger.jaxrs.json");
        packages("com.wordnik.swagger.jersey.listing");

        // register ours resources
        packages("com.hp.tools.judge.web");
        register(CORSResponseFilter.class);

        // register Jackson for custom deserializers
        register(JacksonFeature.class);

        // register the logging
        register(LoggingFilter.class);

        // enable tracing
        property(ServerProperties.TRACING, "ALL");
        property(ServerProperties.TRACING_THRESHOLD, "TRACE");
    }
}
