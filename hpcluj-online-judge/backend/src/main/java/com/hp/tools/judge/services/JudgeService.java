package com.hp.tools.judge.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hp.tools.judge.entities.*;
import com.hp.tools.judge.exceptions.OnlineJudgeException;
import com.hp.tools.judge.utils.Constants;
import org.joda.time.DateTime;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Octavian
 * @since 01.02.2015
 */
public class JudgeService {
    private final static Logger LOGGER = Logger.getLogger(JudgeService.class.getName());

    private Map<Integer, ProgrammingProblem> problems;
    private Map<Integer, List<Test>> testsByProblem;
    private Map<Integer, User> users;
    private Map<String, User> usersByEmail;
    private Map<Integer, Solution> solutions;
    private Map<Integer, List<Solution>> solutionsByProblem;
    private Map<Integer, List<Solution>> solutionsByUser;


    public JudgeService() {
        problems = new HashMap<>();

        try {
            loadProblems();
        } catch (URISyntaxException | IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }

        testsByProblem = new HashMap<>();

        try {
            loadTests();
        } catch (URISyntaxException | IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }

        users = new HashMap<>();
        usersByEmail = new HashMap<>();
        solutions = new ConcurrentHashMap<>(10, 0.75f, Constants.MAX_THREADS);
        solutionsByUser = new HashMap<>();
        solutionsByProblem = new HashMap<>();
    }

    public List<ProgrammingProblemDetails> getAllProgrammingProblemDetails(int userId) throws OnlineJudgeException {
        checkUserId(userId);

        List<ProgrammingProblemDetails> result = new ArrayList<>();

        if (solutionsByUser.containsKey(userId)) {
            Set<Integer> userSolutionsIds = new HashSet<>();
            List<Solution> userSolutions = solutionsByUser.get(userId);
            for (Solution userSolution : userSolutions) {
                userSolutionsIds.add(userSolution.getId());
            }

            for (ProgrammingProblem problem : problems.values()) {
                ProgrammingProblemDetails details = getProgrammingProblemDetails(userSolutionsIds, problem);
                result.add(details);
            }
        } else {
            for (ProgrammingProblem problem : problems.values()) {
                ProgrammingProblemDetails details = createEmptyProgrammingProblemDetails(problem);
                result.add(details);
            }
        }

        return result;
    }

    private ProgrammingProblemDetails createEmptyProgrammingProblemDetails(ProgrammingProblem problem) {
        ProgrammingProblemDetails details = new ProgrammingProblemDetails();
        details.setId(problem.getId());
        details.setTitle(problem.getTitle());
        details.setDescription(problem.getDescription());
        details.setLevel(problem.getLevel());
        details.setMethods(problem.getMethods());

        details.setScore(0f);
        details.setLastResult("N/A");

        return details;
    }

    private ProgrammingProblemDetails getProgrammingProblemDetails(Set<Integer> userSolutionsIds, ProgrammingProblem problem) {
        ProgrammingProblemDetails details = new ProgrammingProblemDetails();
        details.setId(problem.getId());
        details.setTitle(problem.getTitle());
        details.setDescription(problem.getDescription());
        details.setLevel(problem.getLevel());
        details.setMethods(problem.getMethods());

        float maxScore = 0f;
        String lastResult = "N/A";

        List<Solution> problemSolutions = solutionsByProblem.get(problem.getId());
        if (problemSolutions != null) {
            DateTime maxTime = new DateTime(2015, 1, 1, 0, 0);

            for (Solution problemSolution : problemSolutions) {
                if (userSolutionsIds.contains(problemSolution.getId())) {
                    float score = problemSolution.getScore().getValue();
                    if (maxScore < score) {
                        maxScore = score;
                    }

                    DateTime timestamp = problemSolution.getTimestamp();
                    if (maxTime.isBefore(timestamp)) {
                        maxTime = timestamp;
                        lastResult = problemSolution.getScore().toString();
                    }
                }
            }
        }

        details.setScore(maxScore);
        details.setLastResult(lastResult);
        return details;
    }

    public void saveSolution(Solution solution) throws OnlineJudgeException {
        int userId = solution.getUserId();
        checkUserId(userId);

        int problemId = solution.getProblemId();
        checkProblemId(problemId);

        int id = solutions.size();
        solution.setId(id);
        solution.setTimestamp(DateTime.now());
        solution.setScore(new Score(0f, "Not evaluated"));
        solutions.put(id, solution);

        List<Solution> problemSolutions = solutionsByProblem.get(problemId);
        if (problemSolutions == null) {
            problemSolutions = new ArrayList<>();
            solutionsByProblem.put(problemId, problemSolutions);
        }
        problemSolutions.add(solution);

        List<Solution> userSolutions = solutionsByUser.get(userId);
        if (userSolutions == null) {
            userSolutions = new ArrayList<>();
            solutionsByUser.put(userId, userSolutions);
        }
        userSolutions.add(solution);
    }

    public List<Solution> getSolutions(int userId, int problemId) throws OnlineJudgeException {
        checkUserId(userId);
        checkProblemId(problemId);

        List<Solution> result = new ArrayList<>();
        List<Solution> userSolutions = solutionsByUser.get(userId);
        for (Solution userSolution : userSolutions) {
            if (userSolution.getProblemId() == problemId) {
                result.add(userSolution);
            }
        }

        return result;
    }

    public void scoreSolution(int solutionId, AgentService agentService) throws OnlineJudgeException {
        checkSolutionId(solutionId);
        final Solution solution = solutions.get(solutionId);

        AgentInput agentInput = new AgentInput();
        agentInput.setLanguage(solution.getLanguage());
        agentInput.setCode(solution.getCode());
        agentInput.setTestCode(filterTestByProblemId(solution.getProblemId(), solution.getLanguage()));

        agentService.compile(agentInput, new AgentService.Callback() {
            @Override
            public void call(Score score) {
                solution.setScore(score);
            }
        });
    }

    public void saveUser(User user) throws OnlineJudgeException {
        String email = user.getEmail();
        checkUserEmail(email);

        int id = users.size();
        user.setId(id);
        users.put(id, user);
        usersByEmail.put(email, user);
    }

    public User findUserByEmail(String email) {
        return usersByEmail.get(email);
    }

    // Utilities
    private void loadProblems() throws URISyntaxException, IOException {
        final ObjectMapper mapper = new ObjectMapper();

        Files.walkFileTree(getPath(Constants.PROBLEMS_FOLDER), new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                String fileName = file.getFileName().toString();
                int i = fileName.lastIndexOf(".");
                int problemId = Integer.parseInt(fileName.substring(0, i));

                String problemJson = new String(Files.readAllBytes(file), "utf-8");
                ProgrammingProblem problem = mapper.readValue(problemJson, ProgrammingProblem.class);
                problem.setId(problemId);

                problems.put(problem.getId(), problem);

                return FileVisitResult.CONTINUE;
            }
        });
    }

    private void loadTests() throws URISyntaxException, IOException {
        Files.walkFileTree(getPath(Constants.TESTS_FOLDER), new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                String fileName = file.getFileName().toString();
                int i = fileName.lastIndexOf(".");

                Test test = new Test();
                test.setCode(new String(Files.readAllBytes(file), "utf-8"));
                test.setProblemId(Integer.parseInt(fileName.substring(0, i)));
                String extension = fileName.substring(i + 1);
                switch (extension) {
                    case "java":
                        test.setLanguage("java");
                        break;
                    case "py":
                        test.setLanguage("python");
                        break;
                    default:
                        LOGGER.log(Level.WARNING, "Unknown extension: " + extension);
                }

                List<Test> tests = testsByProblem.get(test.getProblemId());
                if (tests == null) {
                    tests = new ArrayList<>();
                    testsByProblem.put(test.getProblemId(), tests);
                }
                tests.add(test);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    private Path getPath(String uri) throws URISyntaxException {
        Path path = Paths.get(uri);
        if (!path.isAbsolute()) {
            path = Paths.get(JudgeService.class.getProtectionDomain().getCodeSource().getLocation().toURI()).resolve(path);
        }
        return path;
    }

    private String filterTestByProblemId(int problemId, String language) {
        List<Test> tests = testsByProblem.get(problemId);
        for (Test test : tests) {
            if (test.getLanguage().equals(language)) {
                return test.getCode();
            }
        }

        return "";
    }

    // preconditions
    private void checkUserId(int userId) throws OnlineJudgeException {
        if (!users.containsKey(userId)) {
            throw new OnlineJudgeException("Invalid user ID: " + userId);
        }
    }

    private void checkUserEmail(String email) throws OnlineJudgeException {
        if (usersByEmail.containsKey(email)) {
            throw new OnlineJudgeException("This email " + email + " is already used");
        }
    }

    private void checkProblemId(int problemId) throws OnlineJudgeException {
        if (!problems.containsKey(problemId)) {
            throw new OnlineJudgeException("Invalid problem ID: " + problemId);
        }
    }

    private void checkSolutionId(int solutionId) throws OnlineJudgeException {
        if (!solutions.containsKey(solutionId)) {
            throw new OnlineJudgeException("Invalid solution ID: " + solutionId);
        }
    }
}
