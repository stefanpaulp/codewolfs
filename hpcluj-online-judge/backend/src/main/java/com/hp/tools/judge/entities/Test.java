package com.hp.tools.judge.entities;

/**
 * @author Octavian
 * @since 21.02.2015
 */
public class Test {
    private int problemId;
    private String language;
    private String code;

    public int getProblemId() {
        return problemId;
    }

    public void setProblemId(int problemId) {
        this.problemId = problemId;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
