package com.hp.tools.judge.evaluators;

import java.util.Locale;

/**
 * @author Octavian
 * @since 23.03.2015
 */
public class OutputEvaluatorFactory {
    public static OutputEvaluator buildEvaluator(String language) {
        switch (language.toLowerCase(Locale.ENGLISH)) {
            case "java":
                return new JavaOutputEvaluator();
            case "python":
                return new PythonOutputEvaluator();
            default:
                throw new IllegalArgumentException("Unknown language");
        }
    }
}
