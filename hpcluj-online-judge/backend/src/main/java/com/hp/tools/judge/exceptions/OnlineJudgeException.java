package com.hp.tools.judge.exceptions;

/**
 * @author Octavian
 * @since 21.02.2015
 */
public class OnlineJudgeException extends Exception {
    public OnlineJudgeException(String message) {
        super(message);
    }
}
