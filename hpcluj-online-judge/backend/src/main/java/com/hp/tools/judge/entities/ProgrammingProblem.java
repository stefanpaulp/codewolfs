package com.hp.tools.judge.entities;

import java.util.List;

/**
 * @author Octavian
 * @since 02.02.2015
 */
public class ProgrammingProblem {
    private int id;
    private String title;
    private String description;
    private String level;
    private List<Method> methods;

    public ProgrammingProblem() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public List<Method> getMethods() {
        return methods;
    }

    public void setMethods(List<Method> methods) {
        this.methods = methods;
    }
}
