# HP CodeWars 2015 - Online Judge #

The application is an online code judging system that presents a user with a problem. The user has to solve the problem
and submit the code. After that the application analyzes the code and gives a score(based on the number of unit tests
that the code passed).

## Overview ##
The application 3 main components are:

* agent - application used to compile and run the unit tests for the code submitted by the user (written in Python)
* backend - server used to manage users, problems and tests (written in Java)
* frontend - web application (written in AngularJS)

## Agent ##
### 1. Details ###

* the agent runs like a server application, listening on port 8080
* it exposes a REST webservice so that other applications can access it (e.g the backend)

### 2. Prerequisites ###
You need the following installed and available in your PATH environment variable:

* Python 2.7.9 (https://www.python.org/downloads/)
* web.py library (http://webpy.org/install)

Also add in the PYTHONPATH environment variable separated by `:` :

* the agent root directory(for example if you have the agent code at C:\agent, then "C:\agent\src" should be added to PYTHONPATH)
* the python default library paths (<python_install_directory>\Lib and <python_install_directory>\Lib)\site-packages

### 3. Build and Run ###
To run the agent start the server using (make sure that the requirements are met, startup.py is in /agent/src/main/server/):
```
#!shell
python startup.py 8080
```

To test the agent functionality use the following request:

	POST http://localhost:8080/agent/test
	Content-Type: application/json
	Data:
	{
		"language":"python",
		"code":"# Here\u0027s our \"unit\".\r\ndef IsOdd(n):\r\n    return n % 2 == 1",
		"testCode":"import unittest\r\n\r\n\r\n# Here\u0027s our \"unit tests\".\r\nclass IsOddTests(unittest.TestCase):\r\n\r\n    def testOne(self):\r\n        self.failUnless(IsOdd(1))\r\n\r\n    def testTwo(self):\r\n        self.failIf(IsOdd(2))\r\n\r\ndef main():\r\n    unittest.main()\r\n\r\nif __name__ == \u0027__main__\u0027:\r\n    main()"
	}

## Backend ##
### 1. Details ###

* the backend runs like a server application, listening on port 8081
* it exposes a REST webservice so that other applications can access it (e.g the frontend)

### 2. Prerequisites ###
You need the following installed and available in your PATH environment variable:

* Java 7 or greater (http://www.oracle.com/technetwork/java/javase/downloads/index.html)
* Apache Maven 3.2.5 or greater (http://maven.apache.org/download.cgi)

### 3. Build and Run ###
Run the following commands in /backend folder:
```
#!shell
# Building
mvn clean package -Dmaven.test.skip=true

# Running
java -jar target/dependency/jetty-runner.jar --port 8081 target/*.war

# or you can use a single command
mvn jetty:run
```

For testing the backend functionality navigate to http://localhost:8081/ and you will see all the possible REST API calls.

## Frontend ##
### 1. Details ###
* the frontend is a SPA (single page application) written in AngularJS
* it uses the Rest Api provided by the backend 

### 2. Prerequisites ###
You need the following installed and available in your PATH environment variable:

* Node.js and node package manager (npm) from https://nodejs.org/download/ (npm is bundled inside nodejs)

### 3. Build and Run ###
Running the app:

* unzip system.zip right inside the frontend/app folder
* run "npm start" inside the frontend directory
* navigate to http://localhost:8000/app/index.html