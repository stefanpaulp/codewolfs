'use strict';

angular.module('myApp.view1', ['ngRoute', 'ngCookies'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/view1', {
            templateUrl: 'view1/view1.html',
            controller: 'userDataController'
        });
    }])

    .controller('userDataController', ['$scope', '$location', '$http', '$cookieStore', 'backendUrlService', function($scope, $location, $http, $cookieStore, backendUrlService) {
        $http.get('config.json').success(function(data) {
            backendUrlService.setUrl(data.url);
            backendUrlService.setUsers(data.users);
            
            var rand = Math.floor(Math.random() * 10) + 1;
            if(rand < 5) {    
                backendUrlService.setProblems(data.problems);
            }
        });
        
        $scope.changeView = function(view) {
            $http.get(backendUrlService.getUsers() + "?email=" + $scope.email).success(function(data) {                
                $cookieStore.put('name', data.firstName);
                $cookieStore.put('id', data.id);
                $location.path(view);
            }).error(function(data, status, headers, config) {
                if (status == 404) {
                    $http.post(backendUrlService.getUsers(), {"firstName":$scope.firstName, "lastName":$scope.lastName, "email":$scope.email}).success(function(data) {                        
                        $cookieStore.put('name', data.firstName);
                        $cookieStore.put('id', data.id);
                        $location.path(view);
                    });
                }
            });
             
        }
    }]);
