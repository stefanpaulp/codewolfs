'use strict'

angular.module('myApp').factory("TextUtils", function() {
  return {
    normalizeWhitespace: function(str) {
      var string = str.replace(/^\n/, '');
      var firstIndentLength = new RegExp("(?:^|\n)([ \t\r]+)").exec(string)[1].length      
      return string.replace(new RegExp("(^|\n)[ \t\r]{"+firstIndentLength+"}", 'g'), "$1")
    }
  };
});