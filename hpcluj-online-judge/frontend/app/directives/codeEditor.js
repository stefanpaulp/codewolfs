'use strict'

angular.module("myApp").directive("codeEditor", function($timeout, TextUtils) {
  return {
    restrict: "E",
    replace: true,
    require: "?ngModel",
    transclude: true,
    scope: {
      syntax: "@",
      theme: "@"
    },
    templateUrl: "components/codeEditor/code-editor.html",
    link: function(scope, element, attrs, ngModelCtrl, transclude) {
    
      var editor = CodeMirror(element[0], {
        mode: scope.syntax || "python",
        theme: scope.theme || "mbo",
        lineNumbers: true
      });
   
      scope.$watch('syntax', function() {
          editor.setOption("mode", scope.syntax);
      });
        
      if (ngModelCtrl) {
        $timeout(function() {
          ngModelCtrl.$render = function() {
            editor.setValue(ngModelCtrl.$viewValue);
          }
        })
      }

      transclude(function(clonedEl) {
        var initialText = TextUtils.normalizeWhitespace(clonedEl.text());
        editor.setValue(scope.$parent.content);
        
        if (ngModelCtrl) {
          $timeout(function() {
            if (initialText && !ngModelCtrl.$viewValue) {
              ngModelCtrl.$setViewValue(scope.$parent.content);
            }

            editor.on('change', function() {
              ngModelCtrl.$setViewValue(editor.getValue());
            });
          });
        }
      });

      scope.$on('$destroy', function() {
        editor.off('change');
      });
    }
  }
});